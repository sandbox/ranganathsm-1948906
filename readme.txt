The Link Tracker module provides functionality to track clicks (currently only the count of clicks) of the hyperlinks present in node content.

A form is shown once a new node is added / edited with all the hyperlinks present in the content.
User can select the hyperlinks for which the count needs to be tracked. Upon submission, the original hyperlink in the content is changed and a mapping is stored in the database.
When any one clicks on the trackable link, it first increments the count in the database for the link and then redirects the user to the intended page.

Administrator can see a report of click counts in the "Reports" section.


Installation:
--------------
Copy the module to your module folder and enable from admin interface.

To Do:
------
1. Provide a feature to search for hyperlinks in all the nodes / selected node containing some keywords and start tracking.
2. Integration with userpoints module to award points to give points to user on click of links.
